# setup for code
import random
import sqlite3

SQLConnect = sqlite3.connect('storage.db')
SQLCursor = SQLConnect.cursor()

# setup for minefield
fieldSize = 8
bombAmount = 10
field = [[0 for x in range(fieldSize)] for y in range(fieldSize)]
bombs = random.sample(range(fieldSize**2), bombAmount)

# make field
for bomb in bombs:
    row = bomb // fieldSize
    col = bomb % fieldSize
    field[row][col] = "B"
    for r in range(max(0, row-1), min(fieldSize, row+2)):
        for c in range(max(0, col-1), min(fieldSize, col+2)):
            if field[r][c] != "B":
                field[r][c] += 1

# show field
def print_field(reveal):
    print("    ", end="")
    for i in range(fieldSize):
        print(f"{i+1:2}", end="")
    print()
    print("  +" + "--" * fieldSize + "-+")
    for i in range(fieldSize):
        print(f"{i+1:2}|", end="")
        for j in range(fieldSize):
            if reveal[i][j]:
                print(f" {field[i][j]}", end="")
            else:
                print("  ", end="")
        print(" |")
    print("  +" + "--" * fieldSize + "-+")

# function for revealing tile
def reveal_tile(row, col, reveal):
    if reveal[row][col]:
        return True
    reveal[row][col] = True
    if field[row][col] == "B":
        print_field(reveal)
        print("Game over!")
        WinOrLoss="Loss"                                                      #game outcome determined as a loss
        return WinOrLoss
    elif field[row][col] > 0:
        field[row][col] = str(field[row][col])
    else:
        field[row][col] = "/"
        for r in range(max(0, row-1), min(fieldSize, row+2)):
            for c in range(max(0, col-1), min(fieldSize, col+2)):
                if field[r][c] == 0:
                    reveal_tile(r, c, reveal)
    print_field(reveal)
    for r in range(fieldSize):
        for c in range(fieldSize):
            if not reveal[r][c] and field[r][c] != "B":
                return True
    print("Winner winner, chicken dinner!")
    WinOrLoss="Win"                                                           #game outcome determined as a win
    return WinOrLoss

# gameplay
reveal = [[False for x in range(fieldSize)] for y in range(fieldSize)]
print_field(reveal)
while True:
    row = int(input("Enter row: ")) - 1
    col = int(input("Enter column: ")) - 1
    if not reveal_tile(row, col, reveal):
        WL=reveal_tile(row, col, reveal)
        break


SQLCursor.execute("INSERT INTO history (Outcome) VALUES (?);",(WL,))
SQLCursor.execute("SELECT * FROM history;")

SQLConnect.commit()
SQLConnect.close()
